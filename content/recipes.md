---
recipes:
  - favourite: false
    image: /static/img/balletjes-tomatensaus.jpg
    ingredients:
      - Gehakt
      - Tomaten in blik
      - Vleesbouillon
      - Ui of sjalot
      - Paneermeel
      - Ei
      - Italiaanse kruiden
    name: Balletjes in tomatensaus
    time: 50
  - image: /static/img/carbonara.jpg
    ingredients:
      - Pasta
      - Spek
      - Parmezaanse kaas
      - Eieren
    name: Pasta carbonara
    time: 30
  - favourite: true
    image: /static/img/pasta-broccoli-zalm.jpg
    ingredients:
      - Pasta
      - Gerookte zalm
      - Kruidenkaas
      - Groentebouillon
      - Broccoli
      - Room
      - Ui of sjalot
    name: Pasta met zalm en broccoli
    time: 30
  - favourite: false
    image: /static/img/wrap-met-kip.jpeg
    ingredients:
      - Wraps
      - Kip
      - Tomaat
      - Ui of sjalot
      - Komkommer
      - Sla
      - Guacamole
    name: Wrap met kip
    time: 20
  - favourite: false
    image: /static/img/spaghetti-bolognaise.jpg
    ingredients:
      - Spaghetti
      - Gehakt
      - Tomaten in blik
      - Wortelen
      - Champignons
      - Ui of sjalot
      - Courgette
      - Paprika
      - Italiaanse kruiden
      - Look
      - Groentebouillon
    name: Spaghetti bolognaise
    time: 60
  - favourite: false
    ingredients:
      - Gehakt
      - Appelmoes
      - Aardappelen
      - Geraspte kaas
    name: Ovenschotel met appelmoes en gehakt
    time: 45
  - favourite: false
    image: /static/img/witloof-in-de-oven.jpg
    ingredients:
      - Witloof
      - Hesp
      - Melk
      - Bloem
      - Aardappelen
      - Geraspte kaas
    name: Witloof in de oven
    time: 60
  - favourite: false
    image: /static/img/kip-rijst-curry.jpg
    ingredients:
      - Kip
      - Rijst
      - Melk
      - Bloem
      - Currypoeder
      - Ui of sjalot
      - Appel
      - Kip- of groentebouillon
    name: Kip met rijst en curry
    time: 45
  - favourite: false
    image: /static/img/croque-monsieur.jpg
    ingredients:
      - Brood
      - Hesp
      - Kaas
    name: Croque Monsieur
    time: 20
  - favourite: false
    image: /static/img/croque-bolognaise.jpg
    ingredients:
      - Brood
      - Hesp
      - Kaas
      - Bolognaisesaus
    name: Croque bolognaise
    time: 20
  - favourite: false
    image: /static/img/vleesbrood-met-krieken.jpg
    ingredients:
      - Gehakt
      - Paneermeel
      - Ei
      - Ui of sjalot
      - Aardappelen
      - Melk
      - Krieken
      - Appelmoes
    name: Fricandon met krieken/appelmoes
    time: 45
  - favourite: false
    ingredients:
      - Blinde vink
      - Erwtjes en wortelen
      - Ui of sjalot
      - Aardappelen
      - Vleesbouillon
      - Maizena
    name: Blinde vink met erwtjes en wortelen
    time: 35
  - favourite: false
    image: /static/img/macaroni.jpg
    ingredients:
      - Macaroni
      - Kaasmix
      - Hesp
      - Bloem
      - Room
    name: Mac & cheese
    time: 60
  - favourite: false
    image: /static/img/lasagna.png
    ingredients:
      - Lasagne uit den Aldi
    name: Lasagne (Aldi)
    time: 40
  - favourite: false
    ingredients:
      - Vleesbouillon
      - Ui of sjalot
      - Pasta
    name: Test recepts
    time: 15
  - favourite: false
    image: /static/img/img_5833.jpg
    ingredients:
      - Ei
      - Ui of sjalot
      - Spek
    name: Quiche Loraine
    time: 100
---

