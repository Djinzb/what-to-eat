import React from 'react';
import styled from 'styled-components';

const Modal = styled.div`
	position: fixed;
	top: 0;
	left: 0;
	margin: 0.5rem;
	width: calc(100vw - 1rem);
	height: calc(100vh - 1rem);
	border-radius: 0.5rem;
	z-index: 10;
	background-color: white;
	box-shadow: 0 0 5px 0 rgba(0, 0, 0, 0.6);
	.closeModal {
		padding: 1rem;
		font-size: 2rem;
		cursor: pointer;
	}
`;

const ModalHeader = styled.div(props => ({
	position: 'relative',
	backgroundImage: `url(${props.bg})`,
	backgroundSize: 'cover',
	width: '100 %',
	height: '30vh',
	borderRadius: '0.5rem 0.5rem 0 0',
}));

const ModalTitle = styled.div`
	position: absolute;
	bottom: 0;
	width: 100%;
	padding: 1rem;
	background-color: rgba(255, 255, 255, 0.8);
	font-size: 2rem;
	font-weight: 400;
	text-align: center;
`;

const ModalContent = styled.div`
	height: calc(70vh - 1rem);
	border-radius: 0 0 0.5rem 0.5rem;
	overflow-y: auto;
	padding: 0.5rem;
`;

const ModalContentTitle = styled.h2`
	width: 100%;
	text-align: center;
	padding: 0.5rem 0;
`;

const IngredientsList = styled.ul`
	list-style-type: none;
	display: grid;
	grid-template-columns: repeat(2, 1fr);
`;

const RecipeInstructions = styled.div`
	text-align: justify;
`;

const recipeDetailModal = ({ setDetailOpen, recipe }) => {
	return (
		<Modal>
			<ModalHeader bg={recipe.image}>
				<div className="closeModal" onClick={() => setDetailOpen(false)}>
					✖️
				</div>
				<ModalTitle>{recipe.name}</ModalTitle>
			</ModalHeader>
			<ModalContent>
				<ModalContentTitle>🥕 Ingrediënten</ModalContentTitle>
				<IngredientsList>
					{recipe.ingredients.map((ingredient, i) => (
						<li key={i}>✔️{ingredient}</li>
					))}
				</IngredientsList>
				<ModalContentTitle>🧾 Recept</ModalContentTitle>
				<RecipeInstructions>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
					eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
					minim veniam, quis nostrud exercitation ullamco laboris nisi ut
					aliquip ex ea commodo consequat.
				</RecipeInstructions>
				<ModalContentTitle>⏲️ Tijd</ModalContentTitle>
				<div>{recipe.time} minuten</div>
			</ModalContent>
		</Modal>
	);
};

export default recipeDetailModal;
