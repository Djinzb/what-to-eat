import React from 'react';
import styled from 'styled-components';

const RecipeTime = styled.div`
	position: absolute;
	bottom: 1px;
	right: 1px;
	font-size: 2rem;
	padding: 0.5rem;
	background-color: white;
	border-radius: 0.5rem;
	img {
		width: 1.2rem;
		margin-right: 0.4rem;
	}
`;

const recipeTime = ({ time }) => {
	return (
		<RecipeTime>
			<small>🕒 </small>
			{time}
			<small>min</small>
		</RecipeTime>
	);
};

export default recipeTime;
