import React from 'react';
import styled from 'styled-components';

const RecipeName = styled.h2`
	text-align: center;
	font-weight: 400;
	margin-bottom: 1rem;
`;

const recipeName = ({ name }) => {
	return <RecipeName>{name}</RecipeName>;
};

export default recipeName;
