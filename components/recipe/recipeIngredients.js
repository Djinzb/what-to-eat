import React from 'react';
import styled from 'styled-components';

const RecipeIngredients = styled.div`
	width: 50%;
	text-align: center;
`;

const recipeIngredients = ({ ingredients }) => {
	return (
		<RecipeIngredients>
			{ingredients.map((ingredient, i) => (
				<div key={`ingredient` + i}>{ingredient}</div>
			))}
		</RecipeIngredients>
	);
};

export default recipeIngredients;
