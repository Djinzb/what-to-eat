import React from 'react';
import styled from 'styled-components';

const TopPickBadge = styled.div`
	position: absolute;
	top: -1.6rem;
	right: -1.2rem;
	transform: rotate(15deg);
	font-size: 4rem;
`;

const topPick = () => {
	return <TopPickBadge>🤤</TopPickBadge>;
};

export default topPick;
