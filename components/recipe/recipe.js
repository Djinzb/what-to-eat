import React from 'react';
import styled from 'styled-components';
import RecipePicture from './recipePicture';
import RecipeName from './recipeName';
import RecipeTime from './recipeTime';
import TopPick from './topPick';

const RecipeCard = styled.div`
	background-color: white;
	position: relative;
	box-shadow: 0 0 3px 0 #506746;
	border-radius: 0.7rem;
	padding: 1rem;
`;

const recipe = ({ recipe, setDetailOpen, setActiveRecipe }) => {
	const provideRecipeDataForModal = () => {
		setDetailOpen(true);
		setActiveRecipe(recipe);
	};
	return (
		<RecipeCard onClick={() => provideRecipeDataForModal()}>
			{recipe.favourite && <TopPick />}
			<RecipeName name={recipe.name}></RecipeName>
			<RecipePicture src={recipe.image} />
			<RecipeTime time={recipe.time}></RecipeTime>
		</RecipeCard>
	);
};

export default recipe;
