import React, { useState } from 'react';
import styled from 'styled-components';
import Recipe from './recipe';
import RecipeDetailModal from './recipeDetailModal';

const RecipesGrid = styled.div`
	--wrapperWidth: calc(100vw - 3rem);
	--gutter: 1.5rem;
	--noOfColumns: 3;
	--rowHeight: calc((var(--wrapperWidth) - (2 * var(--gutter))) / 3);
	z-index: 1;
	margin-top: -5rem;
	width: var(--wrapperWidth);
	display: grid;
	grid-template-columns: repeat(3, 1fr);
	grid-auto-rows: var(--rowHeight);
	grid-gap: var(--gutter);

	@media screen and (max-width: 900px) {
		--wrapperWidth: calc(100vw - 3rem);
		--gutter: 1.5rem;
		--noOfColumns: 1;
		--rowHeight: calc((var(--wrapperWidth) - (2 * var(--gutter))) / 1);
		z-index: 1;
		margin-top: -3rem;
		width: var(--wrapperWidth);
		display: grid;
		grid-template-columns: repeat(1, 1fr);
		grid-auto-rows: var(--rowHeight);
		grid-gap: var(--gutter);
	}
`;

const Recipes = ({ recipes }) => {
	const [detailOpen, setDetailOpen] = useState(false);
	const [activeRecipe, setActiveRecipe] = useState(null);
	return !detailOpen ? (
		<RecipesGrid>
			{recipes.map((recipe, i) => (
				<Recipe
					key={`recipe` + i}
					recipe={recipe}
					setDetailOpen={setDetailOpen}
					setActiveRecipe={setActiveRecipe}
				/>
			))}
		</RecipesGrid>
	) : (
		<RecipeDetailModal setDetailOpen={setDetailOpen} recipe={activeRecipe} />
	);
};

export default Recipes;
