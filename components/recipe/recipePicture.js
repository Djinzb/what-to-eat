import React from 'react';
import styled from 'styled-components';

const Pic = styled.div(props => ({
	backgroundImage: `url(${props.bg})`,
	backgroundSize: 'cover',
	width: '100%',
	height: '10rem',
	borderRadius: '0.5rem',
}));

const recipePicture = ({ src }) => {
	return <Pic bg={src} />;
};

export default recipePicture;
