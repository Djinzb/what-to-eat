import React from 'react';
import styled from 'styled-components';

const SearchBox = styled.input`
	width: 8rem;
	height: 5vh;
	font-size: 2rem;
	text-align: center;
	border: 2px solid white;
	border-width: 0 0 0 0;
	color: white;
	font-weight: 100;
	letter-spacing: 3px;
	background-color: transparent;
	transition: all 0.3s;
	cursor: pointer;
	&:focus {
		@media screen and (max-width: 500px) {
			width: 90vw;
		}
		width: 20rem;
		outline: none;
		border-width: 0 0 2px 0;
	}
	&:focus::placeholder {
		color: transparent;
	}
	@media screen and (max-width: 500px) {
		height: 3rem;
	}
`;

const Search = ({ recipes, setFilteredRecipes }) => {
	const filterRecipes = e => {
		let filtered = recipes.filter(rec => {
			return (
				rec.name.toLowerCase().indexOf(e.target.value.toLowerCase()) !== -1
			);
		});
		setFilteredRecipes(filtered);
		e.preventDefault();
	};

	return (
		<React.Fragment>
			<SearchBox onChange={e => filterRecipes(e)} placeholder="🔍" />
		</React.Fragment>
	);
};

export default Search;
