import React, { useState } from 'react';
import Search from '../components/general/search';
import Recipes from '../components/recipe/recipesGrid';
import styled from 'styled-components';
import content from '../content/recipes.md';

let recipesList = [...content.attributes.recipes];

const RecipePage = styled.div`
	display: flex;
	height: 100vh;
	width: 100vw;
	flex-direction: column;
	font-family: 'HelveticaNeue-UltraLight';
	align-items: center;
`;
const Header = styled.header`
	font-size: 3rem;
	width: 100vw;
	background: rgb(89, 121, 9);
	background: linear-gradient(
		90deg,
		rgba(89, 121, 9, 0.8225665266106442) 18%,
		rgba(0, 139, 12, 1) 100%
	);
	transform: skew(0deg, -10deg);
	padding: 200px 0 300px 0;
	margin-top: -190px;

	.header__content {
		display: flex;
		flex-direction: column;
		align-items: center;
		justify-content: center;
		transform: skew(0deg, 10deg);
		margin-top: 2.5rem;
		text-align: center;
	}

	@media screen and (max-width: 500px) {
		margin-top: -40px;
		padding: 5rem 0 7rem;
		.header__content {
			margin-top: 0;
			&__title {
				font-size: 1.9rem;
			}
		}
	}
`;

const FilterButtonContainer = styled.div`
	display: grid;
	grid-template-columns: repeat(4, 1fr);
	grid-gap: 0.6125rem;
`;

const FilterButton = styled.button`
	width: 3rem;
	height: 3rem;
	border-radius: 0.5rem;
	background-color: white;
	box-shadow: 0 0 5px 0 grey;
	.icon {
		font-size: 1.5rem;
		&__text {
			font-size: 70%;
			margin-top: -0.125rem;
		}
	}
`;

const index = () => {
	const [recipes, setRecipes] = useState(recipesList);
	const [filteredRecipes, setFilteredRecipes] = useState(recipesList);

	const generateRandomNumber = max => {
		return Math.floor(Math.random() * recipesList.length);
	};

	const returnRandomRecipe = (amount = 1) => {
		let randomRecipes = [];

		if (amount == 1) {
			setFilteredRecipes(recipesList[generateRandomNumber(recipesList.lentgh)]);
		}

		for (let i = 0; i < amount; i++) {
			const randomRecipe = generateRandomNumber(recipesList.length);
			if (!randomRecipes.includes(randomRecipe)) {
				randomRecipes.push(
					recipesList.splice(generateRandomNumber(recipesList.length), 1).pop()
				);
			}
		}

		setFilteredRecipes(randomRecipes);
	};

	return (
		<RecipePage>
			<Header>
				<div className="header__content">
					<div className="header__content__title">Maar, wat gaan we eten?</div>
					<Search recipes={recipes} setFilteredRecipes={setFilteredRecipes} />
					<FilterButtonContainer>
						<FilterButton onClick={() => setFilteredRecipes(recipesList)}>
							<div className="icon">📖</div>
							<div className="icon__text">Allemaal</div>
						</FilterButton>
						<FilterButton onClick={() => returnRandomRecipe()}>
							<div className="icon">🤞</div>
							<div className="icon__text">Random</div>
						</FilterButton>
						<FilterButton onClick={() => returnRandomRecipe(2)}>
							<div className="icon">2️⃣</div>
							<div className="icon__text">Twee</div>
						</FilterButton>
						<FilterButton onClick={() => returnRandomRecipe(3)}>
							<div className="icon">3️⃣</div>
							<div className="icon__text">Drie</div>
						</FilterButton>
					</FilterButtonContainer>
				</div>
			</Header>
			<Recipes recipes={filteredRecipes} />
			<style jsx global>{`
				* {
					box-sizing: border-box;
					margin: 0;
					padding: 0;
				}
			`}</style>
		</RecipePage>
	);
};

export default index;
